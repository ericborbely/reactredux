import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';

const navs = [
    { to: '/', name: 'Home' },
    { to: '/about', name: 'About' },
    { to: '/courses', name: 'Courses' }
];

const Header = () => {

    const items = navs.map((n, i) => 
        (<span key={i}><Link to={n.to} activeClassName="active">{n.name}</Link>{" | "}</span>));

    return (<nav>{items}</nav>);
};

export default Header;