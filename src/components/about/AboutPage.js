import React from 'react';
import {Link} from 'react-router';

class AboutPage extends React.Component {
    render() {
        return (
            <div className="jumbotron">
                <h1>About!</h1>
                <Link to="">Go Home</Link>
            </div>
        );
    }
}

export default AboutPage;